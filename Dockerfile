FROM python:3

WORKDIR /build

ADD . /build

RUN pip install sphinx twine && \
    apt-get update && \
    apt-get install sshpass && \
    rm -r /var/lib/apt/lists/*
