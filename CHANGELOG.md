# Change Log

All notable changes to this project will be documented in this file. This
project adheres to [Semantic Versioning](http://semver.org).

## [Unreleased]

### Added
- Add `FilePathOntologyLocator`
- Add `create_world`

### Changed
- `Query#execute` now returns XML Schema datatypes represented by their IRIs as
  strings instead of `None`
- `Instantiation#__init__` now takes a `owlready2.namespace.World` as its first
  parameter as an alternative to a list of ontology locators. An
  `olwready2.namespace.World` object can be created by hand or with the help
  of the new function `create_world`. This makes it possible to load ontologies
  just once for multiple instantiations.

## [1.0.0] - 2019-02-19
