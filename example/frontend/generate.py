# Copyright, 2019, Deutsches Zentrum für Luft- und Raumfahrt e.V.
# Licensed under GPLv3+, see LICENSE for details.
import codecs
import errno
import jinja2
import os

import ontocode

def make_assignable_map(result, _world):
    assignables = {}
    for row in result:
        target = row['target']
        assignable = row['assignable']
        if assignables.get(target):
            assignables[target] += [assignable]
        else:
            assignables[target] = [assignable]
    return { 'assignables': assignables }

def generate():
    ontology_iri = 'http://example.com/ontocode-example/'
    ontology_locator = ontocode.FileSystemOntologyLocator('../ontology/',
                                                          ontology_iri)

    environment = jinja2.Environment(
        loader = jinja2.FileSystemLoader('src-gen')
    )
    quote = lambda s: '\'{0}\''.format(s)
    environment.filters['quote'] = quote
    template = ontocode.Jinja2Template.from_environment(environment,
                                                        'app.jsx.template')

    class_query_string = '''
SELECT ?class
WHERE {
    ?class rdfs:subClassOf <http://example.com/ontocode-example/C001> .
    ?class rdfs:label ?label .
}
ORDER BY DESC(?label)
'''
    class_query = ontocode.Query(class_query_string)
    class_processor_chain = [
        ontocode.ObjectLabelProcessor(),
        ontocode.list_of_dicts_to_dict_of_lists,
    ]
    class_input = ontocode.TemplateInput(class_query, class_processor_chain)

    assignable_query_string = '''
SELECT ?target ?assignable
WHERE {
    ?property rdfs:range ?target .
    ?property rdfs:domain ?assignable .
}
'''
    assignable_query = ontocode.Query(assignable_query_string)
    assignable_processor_chain = [
        ontocode.ObjectLabelProcessor(),
        make_assignable_map,
    ]
    assignable_input = ontocode.TemplateInput(assignable_query,
                                              assignable_processor_chain)

    instantiation = ontocode.Instantiation([ontology_locator],
                                           template,
                                           [class_input, assignable_input])

    result = instantiation.execute_and_write_to_file('src/generated/app.jsx')

generate()
