/*
Copyright, 2019, Deutsches Zentrum für Luft- und Raumfahrt e.V.
Licensed under GPLv3+, see LICENSE for details.
*/
import React, { Component } from 'react';

import { AssignmentList } from './assignment-list.jsx';
import { IdentifiableSummary } from './identifiable-summary.jsx';

export class IdentifiableDetail extends Component {
  render() {
    const identifier = this.props.match.params.identifier;
    return (
      <div>
        <IdentifiableSummary type={this.props.type} identifier={identifier}/>
        {this.props.assignableTypes.map(assignableType => {
          return <AssignmentList key={assignableType}
                                 assigneeType={this.props.type}
                                 assigneeIdentifier={identifier}
                                 assignedType={assignableType}/>;
        })}
      </div>
    );
  }
}
