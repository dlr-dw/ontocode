/*
Copyright, 2019, Deutsches Zentrum für Luft- und Raumfahrt e.V.
Licensed under GPLv3+, see LICENSE for details.
*/
import React, { Component } from 'react';
import { Button, Modal, ModalBody, ModalFooter,
         ModalHeader } from 'reactstrap';

import { IdentifiableForm } from './identifiable-form.jsx';

const EMPTY_IDENTIFIABLE =  {
  identifier: '',
  name: '',
  description: '',
};

const STATE_HIDDEN = {
  callback: () => {},
  identifiable: EMPTY_IDENTIFIABLE,
  open: false,
  operation: '',
  type: '',
};

export class IdentifiableModal extends Component {

  constructor(props) {
    super(props);
    this.state = STATE_HIDDEN;

    this.hide = this.hide.bind(this);
    this.hideAndCallback = this.hideAndCallback.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  show(operation, type, callback, identifiable) {
    identifiable = identifiable || EMPTY_IDENTIFIABLE;
    this.setState({ callback, identifiable, open: true, operation, type });
  }

  hide() {
    this.setState(STATE_HIDDEN);
  }

  hideAndCallback() {
    const identifiable = this.state.identifiable;
    this.hide();
    this.state.callback(identifiable);
  }

  handleChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.value;
    this.setState({
      identifiable: {
        ...this.state.identifiable,
        [name]: value }
    });
  }

  render() {
    return (
      <Modal isOpen={this.state.open} toggle={this.hide}>
        <ModalHeader toggle={this.hide}>
          {this.state.operation} {this.state.type}
        </ModalHeader>
        <ModalBody>
          <IdentifiableForm identifiable={this.state.identifiable}
                            onChange={this.handleChange}/>
        </ModalBody>
        <ModalFooter>
          <Button color='secondary' onClick={this.hide}>Cancel</Button>
          <Button color='primary' onClick={this.hideAndCallback}>
            {this.state.operation}
          </Button>
        </ModalFooter>
      </Modal>
    );
  }

}
