/*
Copyright, 2019, Deutsches Zentrum für Luft- und Raumfahrt e.V.
Licensed under GPLv3+, see LICENSE for details.
*/
import React, { Component } from 'react';
import { Button, Table } from 'reactstrap';

import { IdentifiableModal } from './identifiable-modal.jsx';
import { IdentifiableService } from './service.jsx';

export class IdentifiableSummary extends Component {

  constructor(props) {
    super(props);
    this.state = {
      identifiable: {
        identifier: '',
        name: '',
        description: '',
      },
    };
    this.service = new IdentifiableService(this.props.type);
  }

  componentDidMount() {
    this.loadIdentifiable();
  }

  async loadIdentifiable() {
    const identifiable = await this.service.get(this.props.identifier);
    this.setState({identifiable});
  }

  async update(identifiable) {
    await this.service.update(identifiable);
    this.loadIdentifiable();
  }

  openEditModal() {
    const type = this.props.type;
    const callback = this.update.bind(this);
    const identifiable = this.state.identifiable;
    this.identifiableModal.show('Edit', type, callback, identifiable);
  }

  render() {
    const type = this.props.type;
    const capitalizedType = type.replace(/^(.)/g, l => l.toUpperCase());
    const identifiable = this.state.identifiable;
    return (
      <div className='px-5 py-3'>
        <h1>{capitalizedType} Detail&nbsp;
          <Button color='primary' onClick={this.openEditModal.bind(this)}>
            Edit
          </Button>
        </h1>
        <Table style={{width: 'auto'}}>
          <tbody>
            <tr>
              <th scope="row">Identifier</th>
              <td>{identifiable.identifier}</td>
            </tr>
            <tr>
              <th scope="row">Name</th>
              <td>{identifiable.name}</td>
            </tr>
            <tr>
              <th scope="row">Description</th>
              <td>{identifiable.description}</td>
            </tr>
          </tbody>
        </Table>
        <IdentifiableModal ref={(ref) => this.identifiableModal = ref}/>
      </div>
    );
  };

}
