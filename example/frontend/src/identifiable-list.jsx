/*
Copyright, 2019, Deutsches Zentrum für Luft- und Raumfahrt e.V.
Licensed under GPLv3+, see LICENSE for details.
*/
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, ButtonGroup, ListGroup, ListGroupItem } from 'reactstrap';

import { IdentifiableModal } from './identifiable-modal.jsx';
import { IdentifiableService } from './service.jsx';

export class IdentifiableList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      identifiables: [],
    };
    this.service = new IdentifiableService(this.props.type);
  }

  componentDidMount() {
    this.loadIdentifiables();
  }

  async loadIdentifiables() {
    const identifiables = await this.service.list();
    this.setState({identifiables});
  }

  async delete(identifier) {
    await this.service.delete(identifier);
    this.loadIdentifiables();
  }

  async add(identifiable) {
    await this.service.add(identifiable);
    this.loadIdentifiables();
  }

  openAddModal() {
    const callback = this.add.bind(this);
    this.identifiableModal.show('Add', this.props.type, callback);
  }

  renderEntry(identifiable) {
    const identifier = identifiable.identifier;
    return (
      <ListGroupItem key={identifier} className='p-0'>
        <ButtonGroup className='w-100'>
          <Link className={'btn w-75 text-left'}
                to={`/${this.props.type}s/${identifier}/`}>
            {identifier}
          </Link>
          <Button className={'w-25'} color='danger'
                  onClick={this.delete.bind(this, identifier)}>
            Delete
          </Button>
          </ButtonGroup>
      </ListGroupItem>
    );
  }

  render() {
    const type = this.props.type;
    const capitalizedType = type.replace(/^(.)/g, l => l.toUpperCase());
    return (
      <div className={'px-5 py-3'}>
        <h1>{capitalizedType}s&nbsp;
          <Button color="primary"
                  onClick={this.openAddModal.bind(this)}>
            Add
          </Button>
        </h1>
        <ListGroup>
          {this.state.identifiables.map(this.renderEntry.bind(this))}
        </ListGroup>
        <IdentifiableModal ref={(ref) => this.identifiableModal = ref}/>
      </div>
    );
  }
}
