/*
Copyright, 2019, Deutsches Zentrum für Luft- und Raumfahrt e.V.
Licensed under GPLv3+, see LICENSE for details.
*/
import React, { Component } from 'react';
import { Button, FormGroup, Input, Modal, ModalBody, ModalFooter,
         ModalHeader } from 'reactstrap';

const STATE_HIDDEN = {
  callback: () => {},
  identifiables: [],
  open: false,
  selectedIdentifier: '',
  type: '',
};

export class SelectIdentifiableModal extends Component {

  constructor(props) {
    super(props);
    this.state = STATE_HIDDEN;

    this.hide = this.hide.bind(this);
    this.hideAndCallback = this.hideAndCallback.bind(this);
  }

  show(type, identifiables, callback) {
    const selectedIdentifier = identifiables[0].identifier;
    this.setState({ callback, identifiables, open: true, selectedIdentifier,
                    type });
  }

  hide() {
    this.setState(STATE_HIDDEN);
  }

  hideAndCallback() {
    this.hide();
    this.state.callback(this.state.selectedIdentifier);
  }

  handleChange(event) {
    this.setState({
      selectedIdentifier: event.target.value
    });
  }

  renderOption(identifiable) {
    const identifier = identifiable.identifier;
    return (
      <option key={identifier} value={identifier}>
        {identifier}
      </option>
    );
  }

  render() {
    const type = this.state.type;
    const capitalizedType = type.replace(/^(.)/g, l => l.toUpperCase());
    return (
      <Modal isOpen={this.state.open} toggle={this.hide}>
        <ModalHeader toggle={this.hide}>
          Select {capitalizedType}
        </ModalHeader>
        <ModalBody>
          <form>
            <FormGroup>
              <Input type='select' name='select'
                     onChange={this.handleChange.bind(this)}>
                {this.state.identifiables.map(this.renderOption.bind(this))}
              </Input>
            </FormGroup>
          </form>
        </ModalBody>
        <ModalFooter>
          <Button color='secondary' onClick={this.hide}>Cancel</Button>
          <Button color='primary' onClick={this.hideAndCallback}>
            Select
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}
