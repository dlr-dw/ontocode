# Copyright, 2019, Deutsches Zentrum für Luft- und Raumfahrt e.V.
# Licensed under GPLv3+, see LICENSE for details.
from django.conf.urls import include, url

urlpatterns = [
    url('^', include('iam.generated.urls')),
]
