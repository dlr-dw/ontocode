# Copyright, 2019, Deutsches Zentrum für Luft- und Raumfahrt e.V.
# Licensed under GPLv3+, see LICENSE for details.
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'x&fyaxm=#y*zz(xu6k1o6!dz$g6!0z60)5_721_9)bz12-vtv^'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

INSTALLED_APPS = [
    'django.contrib.contenttypes',

    'rest_framework',
    'corsheaders',

    'iam',
]

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
]

ROOT_URLCONF = 'ontocode_example.urls'

WSGI_APPLICATION = 'ontocode_example.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

REST_FRAMEWORK = {
    'UNAUTHENTICATED_USER' : None,
}

CORS_ORIGIN_WHITELIST = (
    'localhost:3000/'
)
