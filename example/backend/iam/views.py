# Copyright, 2019, Deutsches Zentrum für Luft- und Raumfahrt e.V.
# Licensed under GPLv3+, see LICENSE for details.
from django.core.exceptions import ObjectDoesNotExist
from django.db.utils import IntegrityError
from rest_framework.response import Response
from rest_framework.status import (
    HTTP_201_CREATED,
    HTTP_204_NO_CONTENT,
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_409_CONFLICT,
)

from rest_framework.renderers import JSONRenderer
from rest_framework.views import APIView


class BaseView(APIView):
    renderer_classes = (JSONRenderer,)


class IdentifiableView(BaseView):

    def get_query_set(self):
        return self.Meta.serializer.Meta.model.objects


class IdentifiableListView(IdentifiableView):

    def get(self, request):
        identifiables = self.get_query_set().all()
        serializer = self.Meta.serializer(identifiables, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = self.Meta.serializer(data=request.data)
        if serializer.is_valid():
            try:
                instance = serializer.save()
            except IntegrityError:
                return Response(status=HTTP_409_CONFLICT)
            return Response(serializer.data, status=HTTP_201_CREATED)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


def _with_identifiable(method):

    def wrapped(self, request, id):
        try:
            identifiable = self.get_query_set().get(identifier=id)
            return method(self, request, identifiable)
        except ObjectDoesNotExist:
            return Response(status=HTTP_404_NOT_FOUND)

    return wrapped


class IdentifiableDetailView(IdentifiableView):

    @_with_identifiable
    def get(self, request, identifiable):
        serializer = self.Meta.serializer(instance=identifiable)
        return Response(serializer.data)

    @_with_identifiable
    def put(self, request, identifiable):
        serializer = self.Meta.serializer(instance=identifiable,
                                          data=request.data)
        if serializer.is_valid():
            instance = serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)

    @_with_identifiable
    def delete(self, request, identifiable):
        identifiable.delete()
        return Response(status=HTTP_204_NO_CONTENT)

def _with_identifiables(method):

    def wrapped(self, request, assignee_id, assigned_id):
        try:
            assignee = self.Meta.assignee.objects.get(identifier=assignee_id)
            assigned = self.Meta.assigned.objects.get(identifier=assigned_id)
            return method(self, assignee, assigned)
        except ObjectDoesNotExist:
            return Response(status=HTTP_404_NOT_FOUND)

    return wrapped

class AssignmentView(BaseView):

    @_with_identifiables
    def post(self, assignee, assigned):
        getattr(assignee, self.Meta.assignee_field).add(assigned)
        return Response(status=HTTP_204_NO_CONTENT)

    @_with_identifiables
    def delete(self, assignee, assigned):
        getattr(assignee, self.Meta.assignee_field).remove(assigned)
        return Response(status=HTTP_204_NO_CONTENT)


class AssignmentListView(BaseView):

    def get(self, request, id):
        assignee = self.Meta.assignee.objects.get(identifier=id)
        assigned = getattr(assignee, self.Meta.assigned_field)
        serializer = self.Meta.serializer(assigned, many=True)
        return Response(serializer.data)
