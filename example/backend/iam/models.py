# Copyright, 2019, Deutsches Zentrum für Luft- und Raumfahrt e.V.
# Licensed under GPLv3+, see LICENSE for details.
from django.db import models

IDENTIFIER_REGEX = '[a-zA-Z0-9]+'

class Identifiable(models.Model):
    identifier = models.CharField(max_length=40, unique=True)
    name = models.TextField(null=True)
    description = models.TextField(null=True)

    class Meta:
        abstract = True
