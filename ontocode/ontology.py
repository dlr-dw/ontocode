# Copyright, 2018-2019, Deutsches Zentrum für Luft- und Raumfahrt e.V.
# Licensed under LGPLv3+, see LICENSE for details.
"""
Ontology locators tell OnToCode where to find ontologies. OnToCode uses
Owlready2_ to load ontologies and therefore supports loading `OWL 2.0`_
ontologies in the following formats:

* NTriples
* OWL/XML
* RDF/XML

Ontologies can be loaded from the file system (see
:class:`.FileSystemOntologyLocator` or :class:`.FilePathOntologyLocator`) or
via URL (see :class:`.URLOntologyLocator`).

An ontology locator is created by calling the constructor of a ontology locator
class.

Ontologies are loaded by :ref:`instantiations<instantiations>` for the ontology
locators passed to their constructors. Each
:ref:`instantiation<instantiations>` loads ontologies into its own
:class:`owlready2.namespace.World` object.

.. _`OWL 2.0`: https://www.w3.org/TR/owl-overview/
.. _Owlready2: https://pypi.org/project/Owlready2/
"""
import abc
import os
import owlready2 as owl

__all__ = ['create_world', 'FilePathOntologyLocator',
           'FileSystemOntologyLocator', 'URLOntologyLocator']


def _load_ontology_in_world(world, iri):
    ontology = world.get_ontology(iri)
    ontology.load()


def create_world(ontology_locators):
    """Create world object with preloaded ontologies.

    With `create_world` you can create a single world object for multiple
    instantiations, which is usefull when loading an ontology takes a
    significant amount of time.

    :param list ontology_locators: a list of
        :ref:`ontology locators<ontology-locators>`
    :return: An :ref:`owlready2.namespace.World` object into which the
        ontologies specified via `ontology_locators` have been loaded.
    :rtype: owlready2.namespace.World
    """
    world = owl.World()
    for locator in ontology_locators:
        locator.load(world)
    return world


class _OntologyLocator(metaclass=abc.ABCMeta):
    """Abstract base class for ontology locator classes."""

    @abc.abstractmethod
    def load(self, world):
        """Load ontology into world.

        Loads the ontology, whose location is described by this object, into
        world.

        :param world: an `owlready2.namespace.World` object
        """


class FileSystemOntologyLocator(_OntologyLocator):
    """Specifies an ontology location in file system.

    :param str path: path to directory containing the file that describes
        the ontology with IRI ``iri`` (Not the path to a file).
    :param str iri: IRI of the ontology
    """

    def __init__(self, path, iri):
        self.path = os.path.abspath(path)
        self.iri = iri

    def load(self, world):
        owl.namespace.onto_path = [self.path]
        _load_ontology_in_world(world, self.iri)


class URLOntologyLocator(_OntologyLocator):
    """Specifies an ontology location on the Web.

    :param str url: URL of the ontology
    """

    def __init__(self, url):
        self.url = url

    def load(self, world):
        _load_ontology_in_world(world, self.url)


class FilePathOntologyLocator(URLOntologyLocator):
    """Specifies an ontology location by file path.

    :param str path: path to the file that describes the desired ontology
    """

    def __init__(self, path):
        absolute_path = os.path.abspath(path)
        url = f'file://{absolute_path}'
        super().__init__(url)
