# Copyright, 2018-2019, Deutsches Zentrum für Luft- und Raumfahrt e.V.
# Licensed under LGPLv3+, see LICENSE for details.
"""
OnToCode's queries specify SPARQL_\\-queries that are run by
:ref:`instantiations<instantiations>` against
:ref:`loaded ontologies<ontology-locators>`. A query returns a list of
dictionaries with variable names as keys and Owlready2_ objects as values.
XML Schema datatypes are represented by their IRIs as strings instead of as
Owlready2_ objects.

.. _SPARQL: https://www.w3.org/TR/rdf-sparql-query/
.. _Owlready2: https://pypi.org/project/Owlready2/
"""
__all__ = ['Query']


class Query():
    """Represents a SPARQL_\\-query.

    :param str query: a SPARQL_\\-query
    """

    def __init__(self, query):
        self._query = query

    def execute(self, world):
        """Execute query against world.

        Executes SPARQL_\\-query represented by this instance against ``world``
        and returns the result.

        :param owlready2.namespace.World world: an world object
        :return: a list of dictionaries with variable names as keys and
            Owlready2_ objects as values. XML Schema datatypes are
            represented by their IRIs as strings instead of as Owlready2_
            objects.
        :rtype: list
        """
        graph = world.as_rdflib_graph()
        rdf_result = graph.query(self._query)
        result = [_process_row(row, world) for row in rdf_result]
        return result


def _process_row(row, world):
    return {key: _process_row_entry(row[key], world) for key in row.labels}


XML_SCHEMA_PREFIX = 'http://www.w3.org/2001/XMLSchema#'


def _process_row_entry(entry, world):
    string_representation = str(entry)
    if string_representation.startswith(XML_SCHEMA_PREFIX):
        return string_representation
    return world[string_representation]
